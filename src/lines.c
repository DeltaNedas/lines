#include <libgen.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int individual = 1;
int force = 0;
int verbose = 1;
char** files;

unsigned long lineCount = 0;
unsigned int fileCount = 0;

void printHelp(char* command) {
	printf("Usage: %s [options]\n", command);
	printf("Options are a list of files, or:\n");
	printf("\t-h/--help, shows this message.\n");
	printf("\t-c/--collective, shows line count of all files combined.\n");
	printf("\t-i/--individual, shows line count and name of each file. (default)\n");
	printf("\t-f/--force, skip files that cannot be read rather than aborting.\n");
	printf("\t-v/--verbose, print the settings after argument parsing. (default)\n");
	printf("\t-s/--silent, don't print the settings after argument parsing.\n");
}

char* readFile(FILE* file) {
	char* buffer = NULL;
	long data_size, read_size;

	// Get file size
	fseek(file, 0, SEEK_END);
	data_size = ftell(file);
	rewind(file);

	// Allocate a string of file size + null terminator
	buffer = (char*) malloc(sizeof(char) * (data_size + 1));
	if (buffer == NULL) {
		fprintf(stderr, "\033[31mFailed to allocate memory for file data: %s!\033[0m\n", strerror(errno));
		return NULL;
	}

	read_size = fread(buffer, sizeof(char), data_size, file);

	// Terminate it
	buffer[data_size] = '\0';

	if (data_size != read_size) {
		// I/O Error
		free(buffer);
		buffer = NULL;
		fprintf(stderr, "\033[31mFailed to read data: %s!\033[0m\n", strerror(errno));
		return NULL;
	}

	return buffer;
}

void* allocateFile(char* file) {
	size_t allocated = 0;
	for (int i = 0; i < fileCount; i++) {
		allocated += sizeof(char) * (strlen(files[i]) + 1);
	}

	size_t newSize = allocated + sizeof(char) * (strlen(file) + 1);
	return realloc(files, newSize);
}

int main(int argc, char** argv) {
	if (argc == 1) {
		printHelp(argv[0]);
		exit(1);
	}

	{
		unsigned int skipping = 0;
		files = malloc(sizeof(char));

		for (int i = 1; i < argc; i++) {
			if (skipping) {
				skipping--;
				continue;
			}

			char* arg = argv[i];
			if (!strcmp(arg, "-h") || !strcmp(arg, "--help")) {
				printHelp(argv[0]);
				free(files);
				exit(0);
			} else if (!strcmp(arg, "-c") || !strcmp(arg, "--collective")) {
				individual = 0;
				continue;
			} else if (!strcmp(arg, "-i") || !strcmp(arg, "--individual")) {
				individual = 1;
				continue;
			} else if (!strcmp(arg, "-f") || !strcmp(arg, "--force")) {
				force = 1;
				continue;
			} else if (!strcmp(arg, "-v") || !strcmp(arg, "--verbose")) {
				verbose = 1;
				continue;
			} else if (!strcmp(arg, "-s") || !strcmp(arg, "--silent")) {
				verbose = 0;
				continue;
			}

			if (strlen(basename(arg)) > FILENAME_MAX) {
				fprintf(stderr, "\033[31mFilename at arg %d is too long!\033[0m\n", i);
				free(files);
				exit(1);
			}

			files = allocateFile(arg);
			if (files == NULL) {
				fprintf(stderr, "\033[31mFailed to allocate memory for file list: %s!\033[0m\n", strerror(errno));
				free(files);
				exit(errno);
			}

			files[fileCount++] = arg;
		}
	}

	if (fileCount == 0) {
		printHelp(argv[0]);
		free(files);
		exit(1);
	}

	if (verbose) {
		printf("%s%sChecking %d file%s.\n\n",
			individual ? "Individual file count.\n" : "Collective file count.\n",
			force ? "Skipping unreadable files.\n" : "", fileCount, fileCount == 1 ? "" : "s");
	}

	for (int i = 0; i < fileCount; i++) {
		char* fileName = files[i];
		if (verbose || individual) {
			printf("Checking %s... ", fileName);
			fflush(stdout);
		}

		FILE* file = fopen(fileName, "r+");
		if (file == NULL) {
			fprintf(stderr, "\033[31mFailed to read %s: %s!\033[0m\n", fileName, strerror(errno));
			if (force) {
				continue;
			}
			free(files);
			exit(errno);
		}

		char* data = readFile(file);
		fclose(file);
		if (data == NULL) {
			if (force) {
				continue;
			}
			free(files);
			exit(errno);
		}

		if (individual) {
			unsigned long individualLineCount = 1; // No \n's means it only has one line
			for (int i = 0; i < strlen(data); i++) {
				char c = data[i];
				if (c == '\n') {
					individualLineCount++;
				}
			}
			printf("\033[32mit has %lu lines.\033[0m\n", individualLineCount);
		} else {
			lineCount++;
			for (int i = 0; i < strlen(data); i++) {
				char c = data[i];
				if (c == '\n') {
					lineCount++;
				}
			}
			if (verbose) {
				printf("done!\n");
			}
		}
		free(data);
	}

	if (!individual) {
		printf("\033[32mScanned %u file%s which ha%s a total of %lu line%s.\033[0m\n",
			fileCount, fileCount == 1 ? "" : "s", fileCount == 1 ? "s" : "ve",
			lineCount, lineCount == 1 ? "" : "s");
	}

	free(files);
	exit(0);
}
