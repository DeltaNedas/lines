BUILDDIR = build
ASMDIR = asm
OBJECTDIR = objects
SOURCEDIR = src

CC = gcc
CCFLAGS = -O3 -Wall -ansi -pedantic -I$(SOURCEDIR) --std=c11 -c -g
LDFLAGS = -L$(SOURCEDIR)

EXEC = lines
OBJ = lines
OBJECTS = $(patsubst %, $(OBJECTDIR)/%.o, $(OBJ))
ASM = $(patsubst %, $(ASMDIR)/%.asm, $(OBJ))

BINARIES = /usr/bin

all: $(EXEC)

install:
	cp -f $(BUILDDIR)/$(EXEC) $(BINARIES)/

uninstall:
	rm -f $(BINARIES)/$(EXEC)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%.c
	mkdir -p `dirname $@`
	$(CC) $(CCFLAGS) -o $@ $^

$(ASMDIRs)/%.asm: $(SOURCEDIR)/%.c
	mkdir -p `dirname $@`
	$(CC) -S $(FLAGS) -o $@ $^

$(EXEC): $(OBJECTS)
	mkdir -p $(BUILDDIR)
	$(CC) $(LDFLAGS) -o $(BUILDDIR)/$(EXEC) $^

asm: $(ASM)

clean: uninstall
	rm -rf $(OBJECTDIR)
	rm -rf $(ASMDIR)
	rm -rf $(BUILDDIR)

remake: clean all