# lines
Simple C program that prints line counts of files.
Use --help for more info.

# How to install
```
make
sudo make install
```

# Usage
`lines [options]`
Options are a list of files, or:
	`-h`/`--help`, shows this message.
	`-c`/`--collective`, shows line count of all files combined.
	`-i`/`--individual`, shows line count and name of each file. *(default)*
	`-f`/`--force`, skip files that cannot be read rather than aborting.
	`-v`/`--verbose`, print the settings after argument parsing. *(default)*
	`-s`/`--silent`, don't print the settings after argument parsing.

# Examples
`lines src/lines.c`
`lines * -f # Will error for directories`
